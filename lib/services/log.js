'use strict';

const app = require('../server').app;
const Log = require('log4js');
const _ = require('lodash');

let level = app.get('env') === 'development' ? 'DEBUG' : 'INFO';
let log;

module.exports.error = function () {
    if (log) {
        log.error.apply(log, _.values(arguments));
    }
};

module.exports.info = function () {
    if (log) {
        log.info.apply(log, _.values(arguments));
    }
};

module.exports.debug = function () {
    if (log) {
        log.debug.apply(log, _.values(arguments));
    }
};

module.exports.init = logLevel => {
    log = Log.getLogger('log service');
    log.setLevel(logLevel || level);
};

module.exports.reset = () => { log = null; };
