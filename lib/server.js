'use strict';

const _ = require('lodash');
const express = require('express');
const app = express();
module.exports.app = app;
const exphbs = require('express-handlebars');

const log = require('./services/log');

if (_.isFunction(log.init)) {
    log.init();
}

app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: 'lib/views/layouts',
    partialsDir: 'lib/views/partials'
}));

app.set('view engine', '.hbs');
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/static'));

app.use((req, res, next) => {
    log.debug('Handling request: ' + JSON.stringify({
        url: req.url,
        headers: req.headers,
        method: req.method
    }));

    next();
});

app.get('/', (req, res) => {
    res.render('index.hbs');
});

app.listen(5782, () => {
    log.info('Connected on port 5782');
});
