"use strict";

const chai = require('chai');
let expect = chai.expect;

const sinon = require('sinon');
const log4js = require('log4js');
const sinonChai = require('sinon-chai');
const log = require('../../lib/services/log');
chai.use(sinonChai);

describe("the log service", () => {

    let sandbox;
    let loggerSpy;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();

        loggerSpy = {
            info: { apply: sinon.spy() },
            debug: { apply: sinon.spy() },
            error: { apply: sinon.spy() },
            setLevel: sinon.spy()
        };

        sandbox.stub(log4js, 'getLogger').returns(loggerSpy);
    });

    afterEach(() => {
        sandbox.restore();
        log.reset();
    });

    describe("when initialized", () => {

        beforeEach(() => {
            log.init('override');
        });

        it("should have called getLogger", () => {
            expect(log4js.getLogger).to.have.been.calledWith('log service');
        });

        it("should have called setLevel on the log", () => {
            expect(loggerSpy.setLevel).to.have.been.calledWith('override');
        });

        describe("calling the info function", () => {

            beforeEach(() => {
                log.info('test', 'message');
            });

            it('should pass all the parameters into log.info', () => {
                expect(loggerSpy.info.apply).to.have.been.calledWith(loggerSpy, ['test', 'message']);
            });

        });

        describe("calling the debug function", () => {

            beforeEach(() => {
                log.debug('test', 'message');
            });

            it('should pass all the parameters into log.debug', () => {
                expect(loggerSpy.debug.apply).to.have.been.calledWith(loggerSpy, ['test', 'message']);
            });

        });

        describe("calling the error function", () => {

            beforeEach(() => {
                log.error('test', 'message');
            });

            it('should pass all the parameters into log.error', () => {
                expect(loggerSpy.error.apply).to.have.been.calledWith(loggerSpy, ['test', 'message']);
            });

        });

    });

    describe("when not initialized", () => {

        it("should not have called getLogger", () => {
            expect(log4js.getLogger).not.to.have.been.called;
        });

        it("should not have called setLevel on the log", () => {
            expect(loggerSpy.setLevel).not.to.have.been.called;
        });

        describe("calling the info function", () => {

            beforeEach(() => {
                log.info('test', 'message');
            });

            it('should not call log.info', () => {
                expect(loggerSpy.info.apply).not.to.have.been.called;
            });

        });

        describe("calling the debug function", () => {

            beforeEach(() => {
                log.debug('test', 'message');
            });

            it('should not call log.debug', () => {
                expect(loggerSpy.debug.apply).not.to.have.been.called;
            });

        });

        describe("calling the error function", () => {

            beforeEach(() => {
                log.error('test', 'message');
            });

            it('should not call log.error', () => {
                expect(loggerSpy.error.apply).not.to.have.been.called;
            });

        });

    });

});
