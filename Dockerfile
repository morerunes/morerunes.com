FROM node:latest

CMD [ "npm", "start" ]
WORKDIR /app
ADD . /app

RUN npm install
