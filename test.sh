#!/usr/bin/env bash
npm run lint; result=$?

if [ ${result} -ne 0 ]; then
    exit ${result}
fi

npm run cover; result=$?

exit ${result}
